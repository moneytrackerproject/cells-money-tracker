Polymer({

  is: 'cells-theme-base-catalog',

  properties: {
    /**
     * Path to json file with color information.
     */
    colorsFile: {
      type: String,
      value: ''
    },
    typography: {
      type: String,
      value: ''
    },
    buttons: {
      type: String,
      value: ''
    },
    decorators: {
      type: String,
      value: ''
    },
    inputs: {
      type: String,
      value: ''
    },
    applies: {
      type: String,
      value: ''
    },
    equivalences: {
      type: String,
      value: ''
    },
    heights: {
      type: String,
      value: ''
    },
    radiuses: {
      type: String,
      value: ''
    }
  },

  /**
   * @private
   */
  toggle: function(e) {
    var target = e.currentTarget;

    this.$[target.dataset.collapse].toggle();
    target.classList.toggle('is-opened', this.$[target.dataset.collapse].opened);
  },

  /**
   * @private
   */
  select: function(e) {
    e.target.select();
  },

  toggleAll: function(e) {
    var collapsibles = Polymer.dom(this.root).querySelectorAll('.collapsible__btn');

    for (var i = 0; i < collapsibles.length; i++) {
      collapsibles[i].click();
    }
  }

});
