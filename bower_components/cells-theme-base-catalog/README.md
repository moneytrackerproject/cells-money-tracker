# cells-theme-base-catalog

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

This is the catalog to show the styles, vars and mixins available in the Glomo Theme.

Example:

```html
<cells-theme-base-catalog colors-file="colors.json"></cells-theme-base-catalog>
```

Colors file sample (JSON):

```json
{
  "items": [
    {
      "groupName": "Secondary Yellows",
      "colors": [
        {"colorVar": "--YM0_colour", "hex": "#FDBD2C"},
        {"colorVar": "--YM4_colour", "hex": "#FFFAEB"}
      ]
    }
  ]
}
```

