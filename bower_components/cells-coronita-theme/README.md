![cells-coronita-theme screenshot](cells-theme.svg)

# cells-coronita-theme

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-coronita-theme)

This theme offers you _Coronita_ style guide in a set of variables and mixins to start using it for your components.

__Note__: this theme is used by the Cells Catalog team to style some component demos. 
There is a [generic theme (cells-coronita-base-theme)](https://globaldevtools.bbva.com/bitbucket/projects/CT/repos/cells-coronita-base-theme/browse) with Coronita colors and font-face that must be used instead of this theme.

## Updating colors

Check out [cells-coronita-base-theme](https://globaldevtools.bbva.com/bitbucket/projects/CT/repos/cells-coronita-base-theme/browse/README.md).

## Extend it with project theme

1. Create your `project-theme`
1. Import `cells-coronita-theme`:
  ```json
  {
    "name": "example-project-theme",
    "version": "1.0.0",
    "description": "Extends cells-coronita-theme and includes shared-styles of components",
    "main": [
      "example-project-theme.html"
    ],
    "dependencies": {
      "polymer": "Polymer/polymer#^1.2.0",
      "cells-coronita-theme": "^1.0.0"
    }
  }
  ```

You are ready to go!

> Check [an example](https://globaldevtools.bbva.com/bitbucket/projects/CELLSLABS/repos/example-project-theme/browse)
