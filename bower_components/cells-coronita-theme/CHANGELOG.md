# Changelog

## v11.0.0

Colors and font-face declarations are no longer in this theme. They come from [cells-coronita-base-theme](https://globaldevtools.bbva.com/bitbucket/projects/CT/repos/cells-coronita-base-theme/browse).

## v3.0.0

### Breaking changes

| Removed mixins |
|:---------------|
| --bbva-core-blue-fractal-background |
| --box-rounded |
| --box-rounded-shadow |

### New color names

Vars named `--bbva-teal-*` are now `--bbva-aqua-*`. The old var names are mapped to the new ones for compatibility but they might be removed at some time in the future.

