# cells-checkbox-group

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

`<cells-checkbox-group>` allows to define an accessible set of checkbox buttons using `cells-checkbox-button` component as contents. The checkbox group can have zero or more checkbox buttons currently selected at any time, allowing multiple selection. It's possible to set multiple default selected checkbox buttons using the 'checked' attribute of each checkbox.

Checkbox buttons can be selected/deselected using click and spacebar, and navigated through using the Tab key.

User must provide an icon for the checked version of the checkboxes. Instead of setting a checked icon individually for each checkbox, you can set an `icon-check` attribute in the checkbox group. This will propagate to every children checkbox of the group. You can also use the `icon` attribute in the same way to have all children checkboxes share the same unchecked icon.

`cells-checkbox-group` and its children `cells-checkbox-button`s can be styled using CSS variables and mixins as usual, although one helper class is provided:

- `large`: makes icon checkbox buttons larger.

Examples:

```html
<cells-checkbox-group icon-check="coronita:checkmark" aria-label="Example checkbox group">
	<cells-checkbox-button>Checkbox button 1</cells-checkbox-button>
	<cells-checkbox-button>Checkbox button 2</cells-checkbox-button>
	<cells-checkbox-button>Checkbox button 3</cells-checkbox-button>
</cells-checkbox-group>
```

```html
<cells-checkbox-group icon-check="coronita:checkmark" aria-label="Example checkbox group">
	<cells-checkbox-button checked>Checkbox button 1</cells-checkbox-button>
	<cells-checkbox-button checked>Checkbox button 2</cells-checkbox-button>
	<cells-checkbox-button disabled>Checkbox button 3</cells-checkbox-button>
</cells-checkbox-group>
```

```html
<cells-checkbox-group icon-check="coronita:checkmark" icon="coronita:shopping" aria-label="Example checkbox group">
	<cells-checkbox-button>Checkbox button 1</cells-checkbox-button>
	<cells-checkbox-button>Checkbox button 2</cells-checkbox-button>
	<cells-checkbox-button>Checkbox button 3</cells-checkbox-button>
</cells-checkbox-group>
```

```html
<cells-checkbox-group aria-label="Example checkbox group" class="large" icon-check="coronita:checkmark">
	<cells-checkbox-button icon="coronita:shopping">Checkbox button 1</cells-checkbox-button>
	<cells-checkbox-button icon="coronita:copycard">Checkbox button 2</cells-checkbox-button>
	<cells-checkbox-button icon="coronita:restaurant">Checkbox button 3</cells-checkbox-button>
</cells-checkbox-group>
```

## Accessibility

User should provide an `aria-label` or `aria-labelledby` attribute to the component to ensure correct behavior of assistive technologies like screen readers.

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

| Custom property 											| Description     						| Default   |
|:----------------------------------------------------------|:--------------------------------------|:---------:|
| --cells-checkbox-group 									| Empty mixin 							| {} 		|
| --cells-checkbox-group-checkboxes-content 				| Empty mixin 							| {} 		|
| --cells-checkbox-group-icon-checkboxes-content 			| Empty mixin 							| {} 		|
| --cells-checkbox-button-icon-large-checkbox-size 			| checkbox width & height for .large helper class 	| rem(92px) |
| --cells-checkbox-button-icon-large-checkbox-border-radius | checkbox border radius for .large helper class 	| rem(46px) |
| --cells-checkbox-button-icon-large-label-text-size 		| checkbox label font size for .large helper class | rem(16px) |
| --cells-checkbox-group-large 								| Empty mixin 							| {} 		|