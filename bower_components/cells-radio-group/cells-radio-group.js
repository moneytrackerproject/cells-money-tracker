(function() {

  'use strict';

  var VK_LEFT = 37;
  var VK_UP = 38;
  var VK_RIGHT = 39;
  var VK_DOWN = 40;

  Polymer({

    is: 'cells-radio-group',

    properties: {
      /**
       * Return the list of children enabled radio buttons
       */
      radios: {
        type: Array
      },
      /**
       * Array populated with all radio buttons (enabled or not)
       */
      allRadios: {
        type: Array
      },
      /**
       * Current selected radio button index from the list of enabled radio buttons children of the radio group
       */
      selected: {
        type: Number,
        reflectToAttribute: true,
        notify: true,
        observer: '_setSelected'
      },
      /**
       * Defines if focus must be moved to a new selected radio button. Defaults to false to prevent preselected radio buttons to be focused on page load
       */
      _moveFocus: {
        type: Boolean,
        value: false
      },
      /**
       * If true, allows the radio group to deselect current checked radio button
       */
      allowEmptySelection: {
        type: Boolean,
        observer: '_empty'
      },
      /**
       * Checked icon to propagate to all children radio buttons
       */
      iconCheck: {
        type: String,
        observer: '_iconCheck'
      },
      /**
       * Unchecked icon to propagate to all children radio buttons
       */
      icon: {
        type: String,
        observer: '_icon'
      }
    },

    hostAttributes: {
      'role': 'radiogroup'
    },

    listeners: {
      'keydown': '_handleKeyDown',
      'cells-radio-button-toggled': '_handleSelect'
    },

    attached: function() {
      this.updateRadios();
      if (this.radios.length) {
        this.init();
      }
    },

    detached: function() {
      this.unlisten(this, 'focusout', '_focusOut');
    },

    init: function() {
      this.updateRadios();
      this._removeTabindex();
      this._tabindex(this.selected);
      if (!this.allowEmptySelection) {
        this._empty();
      }
      this.listen(this, 'focusout', '_focusOut');
      this.async(function() {
        this._moveFocus = true;
      });
    },
    /**
     * Propagates group unchecked icon to each child radio button
     */
    _icon: function() {
      this.async(function() {
        for (var i = 0; i < this.allRadios.length; i++) {
          this.allRadios[i].set('icon', this.icon);
        }
      });
    },
    /**
     * Propagates group checked icon to each child radio button
     */
    _iconCheck: function() {
      this.async(function() {
        for (var i = 0; i < this.allRadios.length; i++) {
          this.allRadios[i].set('iconCheck', this.iconCheck);
        }
      });
    },
    /**
     * Updates array lists of the group with current radio buttons
     */
    updateRadios: function() {
      this.allRadios = Polymer.dom(this).querySelectorAll('cells-radio-button');
      this.radios = Polymer.dom(this).querySelectorAll('cells-radio-button:not([disabled])');
    },

    /**
     * Sets tabindex to -1 in the group radio buttons
     */
    _removeTabindex: function() {
      for (var i = 0; i < this.allRadios.length; i++) {
        this.allRadios[i].tabindex = -1;
      }
    },

    /**
     * Updates tabindex property of children radio buttons to allow just one focused item in a group
     */
    _tabindex: function(elem) {
      for (var i = 0; i < this.radios.length; i++) {
        this.radios[i].tabindex = -1;
      }
      if (elem) {
        this.radios[elem].tabindex = 0;
      } else {
        this.radios[0].tabindex = 0;
      }
    },

    /**
     * Sets notEmpty property to radio buttons based on allowEmptySelection property
     */
    _empty: function() {
      if (this.radios) {
        for (var i = 0; i < this.radios.length; i++) {
          this.radios[i].notEmpty = this.allowEmptySelection ? false : true;
        }
      }
    },

    /**
     * Reassigns focus/tabindex to the first radio in the radio group if radio group loses focus without having any radio checked.
     */
    _focusOut: function(e) {
      if (!e.relatedTarget) {
        return;
      }

      for (var i = 0; i < this.radios.length; i++) {
        if (this.radios[i] === e.relatedTarget) {
          var idx = i;
        }
      }

      if (idx === -1 && !this.selected) {
        this._tabindex();
      }
    },

    /**
     * Changes selected radio to a new value
     */
    _setSelected: function(newValue, oldValue) {
      this.async(function() {
        if (oldValue !== undefined && this.radios[oldValue]) {
          this.radios[oldValue].uncheck();
        }
        if (this.radios[newValue]) {
          this.radios[newValue].check();
          if (this._moveFocus) {
            this.radios[newValue].focus();
          }
          this.fire('cells-radio-group-selected-changed', {
            value: newValue,
            oldValue: oldValue
          });
          this._tabindex(newValue);
        }
      });
    },

    /**
     * Handles arrow key events inside the group
     */
    _handleKeyDown: function(e) {
      switch (e.keyCode) {
        case VK_UP:
        case VK_LEFT: {
          this._keyBefore(e);
          break;
        }
        case VK_DOWN:
        case VK_RIGHT: {
          this._keyNext(e);
          break;
        }
      }
    },

    /**
     * Handles selection of previous radio button when up/left arrow keys are pressed
     */
    _keyBefore: function(e) {
      e.preventDefault();
      if (!this.selected || this.selected === 0) {
        this.selected = this.radios.length - 1;
      } else {
        this.selected--;
      }
    },

    /**
     * Handles selection of next radio button when down/right arrow keys are pressed
     */
    _keyNext: function(e) {
      e.preventDefault();
      if (!this.selected) {
        if (this.radios.length > 1) {
          this.selected = 1;
        } else {
          this.selected = 0;
        }
      } else if (this.selected === this.radios.length - 1) {
        this.selected = 0;
      } else {
        this.selected++;
      }
    },

    /**
     * Handles radio selection through click & spacebar in the radio buttons.
     */
    _handleSelect: function(e) {
      for (var i = 0; i < this.radios.length; i++) {
        if (this.radios[i] === Polymer.dom(e).localTarget) {
          var idx = i;
        }
      }

      if (this.selected === idx && this.allowEmptySelection) {
        this.selected = null;
      } else {
        this.selected = idx;
      }
    }

    /**
     * Fired when a new radio button inside the group is selected
     * @event cells-radio-group-selected-changed
     */
  });
}());
