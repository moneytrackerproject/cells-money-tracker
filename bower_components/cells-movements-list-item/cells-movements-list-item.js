// @extends HTMLElement
class CellsMovementsListItem extends Polymer.Element {
  static get is() {
    return 'cells-movements-list-item';
  }

  static get properties() {
    return {
      /**
       * Movement date.
       *
       * @type {String}
       * @default undefined
       */
      date: {
        type: String
      },

      /**
       * Format for movement date.
       *
       * @type {String}
       * @default 'DD/MM/YYYY'
       */
      dateFormat: {
        type: String,
        value: 'DD/MM/YYYY'
      },

      /**
       * Attribute that marks if the current year in the movement date must be shown.
       * Operations made in the current year don't display the year by default.
       *
       * @type {Boolean}
       * @default false
       */
      showCurrentYear: {
        type: Boolean,
        value: false
      },

      /**
       * Movement title.
       *
       * @type {String}
       * @default undefined
       */
      label: {
        type: String
      },

      /**
       * Movement description.
       *
       * @type {String}
       * @default undefined
       */
      description: {
        type: String,
        value: null
      },

      /**
       * Parsed movement amount.
       *
       * Format:
       * ```js
       * {
       *   value: 2.5,
       *   currency: 'EUR'
       * }
       * ```
       *
       * @property parsedAmount
       * @type {Object}
       * @default {}
       */
      parsedAmount: {
        type: Object,
        value: function() {
          return {};
        }
      },

      /**
       * ISO 4217 for the currency.
       *
       * @type {String}
       * @default 'EUR'
       */
      currencyCode: {
        type: String,
        value: 'EUR'
      },

      /**
       * ISO 4217 for the local currency.
       *
       * @type {String}
       * @default 'EUR'
       */
      localCurrency: {
        type: String,
        value: 'EUR'
      },

      /**
       * Current language used to show different punctuation in amounts.
       *
       * @type {String}
       * @default 'es'
       */
      language: {
        type: String,
        value: 'es'
      },

      /**
       * Movement options (badges).
       *
       * Badge structure:
       * ```js
       * {
       *  extraClass: '',
       *  status: 'error' || 'success' || 'default',
       *  label: ''
       * }
       * ```
       *
       * @type {Array}
       * @default []
       */
      badges: {
        type: Array,
        value: function() {
          return [];
        }
      },

      /**
       * Parsed accounting balance.
       * Remaining amount after the current movement.
       *
       * Format:
       * ```js
       * {
       *   value: 2.5,
       *   currency: 'EUR'
       * }
       * ```
       *
       * @property parsedAccountingBalance
       * @type {Object}
       * @default {}
       */
      parsedAccountingBalance: {
        type: Object,
        value: function() {
          return {};
        }
      },

      /**
       * Movement category.
       *
       * @type {Number}
       * @default undefined
       */
      category: {
        type: Number
      },

      /**
       * Category icon size.
       *
       * @type {Number}
       * @default 18
       */
      categoryIconSize: {
        type: Number,
        value: 15
      },

      /**
       * Category description.
       *
       * @type {String}
       * @default undefined
       */
      categoryDescription: {
        type: String
      },

      /**
       * Attribute that indicates if movement category must be hidden.
       *
       * @type {Boolean}
       * @default false
       */
      hideCategory: {
        type: Boolean,
        value: false
      }
    };
  }
}

customElements.define(CellsMovementsListItem.is, CellsMovementsListItem);
