# CHANGELOG

## Update to v1.1.0

- CSS changes in component, add Coronita Styles.

- Added css styles for disabled steps.

- Added in demo custom-style.

- Added iconRight property

- Added in demo `demo snippet` for demos, (copy and paste) and `dropdown` for select differents demos

- Removed `cells-demo-mobile` and `paper-tabs` from demo.

- Added in demo `cells-demo-event-toaster` and remove `paper-toast`.

- Removed `single element`example demo.

- Changes in demo examples for better visualization of demos.

- Added tag `a11y`.
