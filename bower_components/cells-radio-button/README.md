# cells-radio-button

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

`cells-radio-button` generates a custom radio button, aimed to be used inside a `cells-radio-group` component.

The component has five possibles classes.

- If you don´t add any class is the default component, and it is used when the component is used with white background.
- The class `grey` is used when the component is used with grey background (#F4F4F4).
- The class `light-blue` is used when the component is used with blue background (#1973B8).
- The class `core-blue` is used when the component is used with default blue background (#0A5393).
- The class `dark-blue` is used when the component is used with dark blue background (#004481).

Example:
```html
<cells-radio-button class="grey">Label for the radio button</cells-radio-button>
<cells-radio-button class="light-blue">Label for the radio button</cells-radio-button>
<cells-radio-button class="core-blue">Label for the radio button</cells-radio-button>
<cells-radio-button class="dark-blue">Label for the radio button</cells-radio-button>
```

The states of the radio can be:

- Checked or unchecked
- disabled or enabled
- readonly
- with error

```html
<cells-radio-button checked>Label for the radio button</cells-radio-button>
```

```html
<cells-radio-button disabled>Label for the radio button</cells-radio-button>
```

```html
<cells-radio-button readonly checked>Example Radio button</cells-radio-button>
```

```html
<cells-radio-button error error-icon="coronita:alert" error-message="error message">Example Radio button</cells-radio-button>
```

Full accessibility features are provided when using it inside a `cells-radio-group`.

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                                              | Selector                                                           | CSS Property     | Value                                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------------ | ---------------- | ---------------------------------------------------------------- |
| --cells-fontDefault                                          | :host                                                              | font-family      |  sans-serif                                                      |
| --cells-radio-button-radio-size                              | .radio                                                             | width            |  1.5rem                                                          |
| --cells-radio-button-radio-size                              | .radio                                                             | height           |  1.5rem                                                          |
| --cells-radio-button-radio-background-color                  | .radio                                                             | background-color | --bbva-100                                                       |
| --cells-radio-button-radio-border-radius                     | .radio                                                             | border-radius    |  50%                                                             |
| --cells-radio-button-radio-border-color                      | .radio                                                             | border           | --bbva-500                                                       |
| --cells-radio-button-hover-radio-background-color            | .radio:hover                                                       | background-color | --bbva-white                                                     |
| --cells-radio-button-label-text-size                         | .label                                                             | font-size        |  0.9375rem                                                       |
| --cells-radio-button-label-color                             | .label                                                             | color            | --bbva-600                                                       |
| --bbva-white-core-blue                                       | :host(:focus) .radio                                               | box-shadow       |  ![#1464A5](https://placehold.it/15/1464A5/000000?text=+) #1464A5 |
| --cells-radio-button-radio-check-size                        | :host([aria-checked="true"]) .radio:after                          | width            |  0.75rem                                                         |
| --cells-radio-button-radio-check-size                        | :host([aria-checked="true"]) .radio:after                          | height           |  0.75rem                                                         |
| --cells-radio-button-radio-check-background-color            | :host([aria-checked="true"]) .radio:after                          | background-color | --bbva-core-blue                                                 |
| --cells-radio-button-radio-check-border-radius               | :host([aria-checked="true"]) .radio:after                          | border-radius    |  50%                                                             |
| --cells-radio-button-grey-radio-background-color             | :host(.grey) .radio                                                | background-color | --bbva-white                                                     |
| --cells-radio-button-grey-hover-radio-background-color       | :host(.grey) .radio:hover                                          | background-color | --bbva-100                                                       |
| --cells-radio-button-blue-radio-border                       | :host([class$="blue"]) .radio                                      | border           | --bbva-white                                                     |
| --cells-radio-button-blue-label-color                        | :host([class$="blue"]) .label                                      | color            | --bbva-white                                                     |
| --cells-radio-button-blue-checked-color                      | :host([class$="blue"][aria-checked="true"]) .radio:after           | background-color | --bbva-white                                                     |
| --cells-radio-button-blue-focus-radio-box-shadow             | :host([class$="blue"]:focus) .radio                                | box-shadow       | --bbva-white                                                     |
| --cells-radio-button-blue-readonly-color                     | :host([class$="blue"][readonly][aria-checked="true"]) .radio       | border           | --bbva-300                                                       |
| --cells-radio-button-blue-readonly-color                     | :host([class$="blue"][readonly][aria-checked="true"]) .radio:after | color            | --bbva-300                                                       |
| --cells-radio-button-blue-readonly-color                     | :host([class$="blue"][readonly][aria-checked="true"]) .label       | color            | --bbva-300                                                       |
| --cells-radio-button-error-blue-background-color             | :host([class$="blue"][error])                                      | background-color | --bbva-red-white                                                 |
| --cells-radio-button-error-blue-border-color                 | :host([class$="blue"][error])                                      | border           | --bbva-red-dark                                                  |
| --cells-radio-button-error-blue-radio-background-color       | :host([class$="blue"][error]) .radio                               | background-color | --bbva-white                                                     |
| --cells-radio-button-error-blue-radio-border-color           | :host([class$="blue"][error]) .radio                               | border           | --bbva-red-dark                                                  |
| --cells-radio-button-error-blue-radio-hover-background-color | :host([class$="blue"][error]) .radio:hover                         | background-color | --bbva-white                                                     |
| --cells-radio-button-light-blue-radio-background-color       | :host(.light-blue) .radio                                          | background-color |  ![#1973B8](https://placehold.it/15/1973B8/000000?text=+) #1973B8 |
| --cells-radio-button-light-blue-radio-hover-background-color | :host(.light-blue) .radio:hover                                    | background-color | --bbva-dark-medium-blue                                          |
| --cells-radio-button-core-blue-radio-background-color        | :host(.core-blue) .radio                                           | background-color |  ![#0A5393](https://placehold.it/15/0A5393/000000?text=+) #0A5393 |
| --cells-radio-button-core-blue-radio-hover-background-color  | :host(.core-blue) .radio:hover                                     | background-color | --bbva-core-blue                                                 |
| --cells-radio-button-dark-blue-radio-background-color        | :host(.dark-blue) .radio                                           | background-color | --bbva-core-blue                                                 |
| --cells-radio-button-dark-blue-radio-hover-background-color  | :host(.dark-blue) .radio:hover                                     | background-color | --bbva-dark-core-blue                                            |
| --cells-radio-button-disabled-opacity                        | :host([aria-disabled="true"])                                      | opacity          |  0.3                                                             |
| --cells-radio-button-readonly-point                          | :host([readonly]) .radio:after                                     | background-color | --bbva-500                                                       |
| --cells-radio-button-error-radio-background-color            | :host([error]) .radio                                              | background-color | --bbva-red-white                                                 |
| --cells-radio-button-error-radio-border-color                | :host([error]) .radio                                              | border-color     | --bbva-red-dark                                                  |
| --cells-radio-button-error-hover-radio-background-color      | :host([error]) .radio:hover                                        | background-color | --bbva-white                                                     |
| --cells-radio-button-error-label-color                       | :host([error]) .label                                              | color            | --bbva-red-dark                                                  |
| --cells-radio-button-icon-radio-size                         | :host([icon]) .radio                                               | width            |  3.125rem                                                        |
| --cells-radio-button-icon-radio-size                         | :host([icon]) .radio                                               | height           |  3.125rem                                                        |
| --cells-radio-button-icon-radio-border-radius                | :host([icon]) .radio                                               | border-radius    |  50%                                                             |
| --cells-radio-button-icon-radio-background-color             | :host([icon]) .radio                                               | background-color | --bbva-100                                                       |
| --cells-radio-button-icon-radio-border-color                 | :host([icon]) .radio                                               | border           |  transparent                                                     |
| --cells-radio-button-icon-icon-size                          | :host([icon]) cells-atom-icon                                      | width            |  1.625rem                                                        |
| --cells-radio-button-icon-icon-size                          | :host([icon]) cells-atom-icon                                      | height           |  1.625rem                                                        |
| --cells-radio-button-icon-icon-color                         | :host([icon]) cells-atom-icon                                      | color            | --bbva-500                                                       |
| --cells-radio-button-icon-label-color                        | :host([icon]) .label                                               | color            | --bbva-500                                                       |
| --cells-radio-button-icon-radio-check-background-color       | :host([icon][aria-checked="true"]) .radio                          | background-color | --bbva-core-blue                                                 |
| --cells-radio-button-icon-check-icon-color                   | :host([icon][aria-checked="true"]) cells-atom-icon                 | color            | --bbva-white                                                     |
| --cells-radio-button-icon-check-label-color                  | :host([icon][aria-checked="true"]) .label                          | color            | --bbva-core-blue                                                 |
### @apply
| Mixins                                           | Selector                                                           | Value |
| ------------------------------------------------ | ------------------------------------------------------------------ | ----- |
| --cells-radio-button                             | :host                                                              | {}    |
| --cells-radio-button-radio                       | .radio                                                             | {}    |
| --cells-radio-button-hover-radio                 | .radio:hover                                                       | {}    |
| --cells-radio-button-body-label                  | .body-label                                                        | {}    |
| --cells-radio-button-label                       | .label                                                             | {}    |
| --cells-radio-button-focus                       | :host(:focus)                                                      | {}    |
| --cells-radio-button-focus-radio                 | :host(:focus) .radio                                               | {}    |
| --cells-radio-button-checked                     | :host([aria-checked="true"])                                       | {}    |
| --cells-radio-button-radio-checked               | :host([aria-checked="true"]) .radio:after                          | {}    |
| --cells-radio-button-grey-radio                  | :host(.grey) .radio                                                | {}    |
| --cells-radio-button-grey-radio-hover            | :host(.grey) .radio:hover                                          | {}    |
| --cells-radio-button-blue-radio                  | :host([class$="blue"]) .radio                                      | {}    |
| --cells-radio-button-blue-label                  | :host([class$="blue"]) .label                                      | {}    |
| --cells-radio-button-blue-checked                | :host([class$="blue"][aria-checked="true"]) .radio:after           | {}    |
| --cells-radio-button-blue-focus-radio            | :host([class$="blue"]:focus) .radio                                | {}    |
| --cells-radio-button-blue-readonly-radio         | :host([class$="blue"][readonly][aria-checked="true"]) .radio       | {}    |
| --cells-radio-button-blue-readonly-icon_checked  | :host([class$="blue"][readonly][aria-checked="true"]) .radio:after | {}    |
| --cells-radio-button-blue-readonly-label         | :host([class$="blue"][readonly][aria-checked="true"]) .label       | {}    |
| --cells-radio-button-error-blue                  | :host([class$="blue"][error])                                      | {}    |
| --cells-radio-button-error-blue-radio            | :host([class$="blue"][error]) .radio                               | {}    |
| --cells-radio-button-error-blue-radio-hover      | :host([class$="blue"][error]) .radio:hover                         | {}    |
| --cells-radio-button-light-blue-radio            | :host(.light-blue) .radio                                          | {}    |
| --cells-radio-button-light-blue-radio-hover      | :host(.light-blue) .radio:hover                                    | {}    |
| --cells-radio-button-core-blue-radio             | :host(.core-blue) .radio                                           | {}    |
| --cells-radio-button-core-blue-radio-hover       | :host(.core-blue) .radio:hover                                     | {}    |
| --cells-radio-button-dark-blue-radio             | :host(.dark-blue) .radio                                           | {}    |
| --cells-radio-button-dark-blue-radio-hover       | :host(.dark-blue) .radio:hover                                     | {}    |
| --cells-radio-button-disabled                    | :host([aria-disabled="true"])                                      | {}    |
| --cells-radio-button-readonly                    | :host([readonly])                                                  | {}    |
| --cells-radio-button-readonly-radio              | :host([readonly]) .radio:after                                     | {}    |
| --cells-radio-button-error                       | :host([error])                                                     | {}    |
| --cells-radio-button-error-radio                 | :host([error]) .radio                                              | {}    |
| --cells-radio-button-error-radio-hover           | :host([error]) .radio:hover                                        | {}    |
| --cells-radio-button-error-label                 | :host([error]) .label                                              | {}    |
| --cells-radio-button-icon                        | :host([icon])                                                      | {}    |
| --cells-radio-button-icon-radio                  | :host([icon]) .radio                                               | {}    |
| --cells-radio-button-icon-icon                   | :host([icon]) cells-atom-icon                                      | {}    |
| --cells-radio-button-icon-icon_checked           | :host([icon]) .icon-checked                                        | {}    |
| --cells-radio-button-icon-icon_unchecked         | :host([icon]) .icon-unchecked                                      | {}    |
| --cells-radio-button-icon-body-label             | :host([icon]) .body-label                                          | {}    |
| --cells-radio-button-icon-label                  | :host([icon]) .label                                               | {}    |
| --cells-radio-button-icon-checked                | :host([icon][aria-checked="true"])                                 | {}    |
| --cells-radio-button-icon-radio-checked          | :host([icon][aria-checked="true"]) .radio                          | {}    |
| --cells-radio-button-icon-icon-checked           | :host([icon][aria-checked="true"]) cells-atom-icon                 | {}    |
| --cells-radio-button-icon-icon_checked-checked   | :host([icon][aria-checked="true"]) .icon-checked                   | {}    |
| --cells-radio-button-icon-icon_unchecked-checked | :host([icon][aria-checked="true"]) .icon-unchecked                 | {}    |
| --cells-radio-button-icon-label-checked          | :host([icon][aria-checked="true"]) .label                          | {}    ||
