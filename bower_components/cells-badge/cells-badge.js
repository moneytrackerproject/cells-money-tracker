class CellsBadge extends Polymer.Element {
  static get is() {
    return 'cells-badge';
  }

  static get properties() {
    return {
      /**
       * Badge text label.
       *
       * @property label
       * @type {String}
       */
      label: {
        type: String
      }
    };
  }
}

customElements.define(CellsBadge.is, CellsBadge);