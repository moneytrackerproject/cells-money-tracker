![cells-badge screenshot](cells-badge.png)

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](https://au-bbva-ether-cellscatalogs.appspot.com/)

[Demo of component in Cells Catalog](https://au-bbva-ether-cellscatalogs.appspot.com/?view=docs#/component/cells-badge)

# &lt;cells-badge&gt;

`<cells-badge>` displays a text badge with different styles depending on the used class.

Example:

```html
<cells-badge label="Financiable"></cells-badge>
```

__Available classes:__

* _The component used without any class has blue color and border._

| Class name    | Color     | Style                                                                          |
|:--------------|:----------|:-------------------------------------------------------------------------------|
| `success`     | green     | -                                                                              |
| `warning`     | orange    | -                                                                              |
| `error`       | red       | -                                                                              |
| `neutral`     | dark grey | -                                                                              |
| `fill`        | -         | inverted colors (text in white and background in the color set by other class) |
| `full-radius` | -         | rounded corners                                                                |

## Styling

The following custom properties and mixins are available for styling:

| Custom Property                | Description                                        | Default                              |
| :----------------------------- | :------------------------------------------------- | :----------------------------------- |
| --cells-badge                  | Mixin applied to :host                             | {}                                   |
| --cells-badge-default-color    | default border and text color                      | var(--bbva-white-core-blue, #1464A5) |
| --cells-badge-error-color      | border and text color for :host(.error)            | var(--bbva-dark-coral, #D44B50)      |
| --cells-badge-neutral-color    | border and text color for :host(.neutral)          | var(--bbva-500, #666666)             |
| --cells-badge-success-color    | border and text color for :host(.success)          | var(--bbva-green, #48AE64)           |
| --cells-badge-warning-color    | border and text color for :host(.warning)          | var(--bbva-orange, #F7893B)          |
| --cells-badge-fill-color       | text color for :host(.fill)                        | var(--bbva-white, #FFFFFF)           |
| --cells-badge-full-radius      | Mixin applied to :host(.full-radius)               | {}                                   |
| --cells-badge-full-radius-span | Mixin applied to inner span in :host(.full-radius) | {}                                   |
| --cells-badge-span             | Mixin applied to inner span                        | {}                                   |
