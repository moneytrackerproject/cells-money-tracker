class CellsCategoryIcon extends Polymer.Element {
  static get is() {
    return 'cells-category-icon';
  }

  static get properties() {
    return {
      /**
       * Value that represents the category of the icon.
       */
      category: {
        type: Number,
        value: null
      },

      /**
       * Icon size.
       * @default 18px (in component styles).
       */
      size: {
        type: Number,
        observer: '_updateIconSize'
      },

      /**
       * Name of the used iconset.
       */
      iconset: {
        type: String,
        value: 'coronita'
      },

      /**
       * Correspondence map between icon names and categories.
       *
       * Format:
       *
       * ```js
       * {
       *   0: 'iconname',
       *   1: 'someicon'
       * }
       * ```
       */
      categoryIconMap: {
        type: Object,
        value: function() {
          return {
            0: 'close',
            1: 'currencyexchange',
            2: 'investment',
            3: 'build',
            4: 'donation',
            5: 'bank',
            6: 'retail',
            7: 'supermarket',
            8: 'education',
            9: 'cash',
            10: 'mortaje',
            11: 'retirement',
            12: 'discount',
            13: 'health',
            14: 'insurance',
            15: 'auto',
            78: 'bizum',
            999: 'close',
            9999: 'sales',
            9998: 'sales'
          };
        }
      },

      _icon: {
        type: String,
        computed: '_computeIcon(category, iconset)'
      },
    };
  }

  _computeIcon(category, iconset) {
    if (iconset) {
      return iconset + ':' + this.categoryIconMap[category];
    }

    return this.categoryIconMap[category];
  }

  _updateIconSize(size) {
    this.updateStyles({'--cells-category-icon-size': `${size}px`});
  }
}

customElements.define(CellsCategoryIcon.is, CellsCategoryIcon);
