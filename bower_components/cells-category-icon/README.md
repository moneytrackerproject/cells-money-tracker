![cells-category-icon screenshot](cells-category-icon.png)

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](https://au-bbva-ether-cellscatalogs.appspot.com/)

[Demo of component in Cells Catalog](https://au-bbva-ether-cellscatalogs.appspot.com/?view=docs#/component/cells-category-icon)

# &lt;cells-category-icon&gt;

`<cells-category-icon>` displays a colored icon that depends on the associated icon for a category. The category association is set in the `categoryIconMap` property.

The size of the icon can be set declaratively using the `size` property with a numeric value or by setting the
CSS custom property `--cells-category-icon-size` with a value and a unit. Example (18px).

If you use different keys that the default ones for the category association, you should take into account that the
existing classes used to set the icon colors won't match your keys. You will need to define your custom classes in a shared style
with ID `cells-category-icon-shared-styles`.

Example:

```html
<cells-category-icon category="2"></cells-category-icon>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom Property                  | Description                 | Default                                |
| :------------------------------- | :-------------------------- | :------------------------------------- |
| --cells-category-icon-size       | icon size                   | 1.125rem                               |
| --cells-category-icon            | Mixin applied to :host      | {}                                     |
| --cells-category-icon-icon       | empty mixin applied to icon | {}                                     |
| --cells-category-icon-color-0    | color for category 0        | var(--bbva-300, #D3D3D3)             |
| --cells-category-icon-color-1    | color for category 1        | var(--bbva-green, #48AE64)           |
| --cells-category-icon-color-2    | color for category 2        | var(--bbva-500, #666666)             |
| --cells-category-icon-color-3    | color for category 3        | var(--bbva-light-red, #E77D8E)       |
| --cells-category-icon-color-4    | color for category 4        | var(--bbva-light-purple, #B6A8EE)    |
| --cells-category-icon-color-5    | color for category 5        | var(--bbva-white-core-blue, #1464A5) |
| --cells-category-icon-color-6    | color for category 6        | var(--bbva-pink, #F78BE8)            |
| --cells-category-icon-color-7    | color for category 7        | var(--bbva-aqua, #2DCCCD)            |
| --cells-category-icon-color-8    | color for category 8        | var(--bbva-purple, #8F7AE5)          |
| --cells-category-icon-color-9    | color for category 9        | var(--bbva-light-green, #88CA9A)     |
| --cells-category-icon-color-10   | color for category 10       | var(--bbva-orange, #F7893B)          |
| --cells-category-icon-color-11   | color for category 11       | var(--bbva-yellow, #F8CD51)          |
| --cells-category-icon-color-12   | color for category 12       | var(--bbva-light-purple, #B6A8EE)    |
| --cells-category-icon-color-13   | color for category 13       | var(--bbva-coral, #F35E61)           |
| --cells-category-icon-color-14   | color for category 14       | var(--bbva-light-blue, #5BBEFF)      |
| --cells-category-icon-color-15   | color for category 15       | var(--bbva-dark-gold, #B79E5E)       |
| --cells-category-icon-color-78   | color for category 16       | var(--bbva-500, #666666)             |
| --cells-category-icon-color-999  | color for category 999      | var(--bbva-300, #D3D3D3)             |
| --cells-category-icon-color-9999 | color for category 9999     | var(--bbva-dark-core-blue, #043263)  |
| --cells-category-icon-color-9998 | color for category 9998     | var(--bbva-dark-core-blue, #043263)  |
