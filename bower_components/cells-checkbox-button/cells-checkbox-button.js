(function() {
  'use strict';
  /**
   * @customElement
   * @polymer
   * @demo demo/index.html
   */
  Polymer({

    is: 'cells-checkbox-button',

    properties: {
      /**
       * tabindex property of the checkbox button
       */
      tabindex: {
        type: Number,
        value: 0,
        reflectToAttribute: true,
      },
      /**
       * Checked status of the checkbox button
       */
      checked: {
        type: Boolean,
        value: false,
        notify: true,
        reflectToAttribute: true,
        observer: '_checked',
      },
      /**
       * Icon for the checkbox button when it's unchecked
       */
      icon: {
        type: String,
        reflectToAttribute: true,
      },

      /**
       * Icon for the checkbox button when it's checked
       */
      iconCheck: {
        type: String,
        reflectToAttribute: true,
        value: 'coronita:checkmark',
      },
      /**
       * Disabled status of the checkbox button
       */
      disabled: {
        type: Boolean,
        reflectToAttribute: true,
        notify: true,
        observer: '_disabled',
      },
      /**
       * readonly status of the radio button
       */
      readonly: {
        type: Boolean,
        reflectToAttribute: true,
        notify: true,
      },
      /**
       * error status of the radio button
       */
      error: {
        type: Boolean,
        reflectToAttribute: true,
        notify: true,
      },
      /**
       * Defines icon of error
       */
      errorIcon: {
        type: String,
        value: null,
      },
      /**
       * Defines error message
       */
      errorMessage: {
        type: String,
        value: null,
      },
    },
    hostAttributes: {
      'role': 'checkbox',
      'aria-checked': 'false',
    },
    listeners: {
      'keydown': '_handleKeyDown',
      'tap': 'toggleCheck',
    },
    /**
     * Updates aria-checked attribute and fires checked/unchecked events
     * @param {Boolean} newValue
     */
    _checked: function(newValue) {
      this.setAttribute('aria-checked', this.checked);
      if (newValue) {
        this.dispatchEvent(new CustomEvent('cells-checkbox-button-checked', {
          bubbles: true,
          composed: true,
        }));
      } else {
        this.dispatchEvent(new CustomEvent('cells-checkbox-button-unchecked', {
          bubbles: true,
          composed: true,
        }));
      }
    },
    /**
     * Updates aria-disabled attribute and fires disabled/enabled events
     * @param {Boolean} newValue
     */
    _disabled: function(newValue) {
      this.setAttribute('aria-disabled', this.disabled);
      if (newValue) {
        this.dispatchEvent(new CustomEvent('cells-checkbox-button-disabled', {
          bubbles: true,
          composed: true,
        }));
      } else {
        this.dispatchEvent(new CustomEvent('cells-checkbox-button-enabled', {
          bubbles: true,
          composed: true,
        }));
      }
    },
    /**
     * Checks the checkbox button
     */
    check: function() {
      if (!this.checked) {
        this.checked = !this.checked;
      }
    },
    /**
     * Unchecks the checkbox button
     */
    uncheck: function() {
      if (this.checked) {
        this.checked = !this.checked;
      }
    },
    /**
     * Toggles the checkbox button checked state
     */
    toggleCheck: function() {
      if (!this.disabled) {
        this.checked = !this.checked;
        this.dispatchEvent(new CustomEvent('cells-checkbox-button-toggled', {
          bubbles: true,
          composed: true,
          detail: { checked: this.checked, },
        }));
      }
    },
    /**
     * Enables the checkbox button
     */
    enable: function() {
      if (this.disabled) {
        this.disabled = !this.disabled;
      }
    },
    /**
     * Disables the checkbox button
     */
    disable: function() {
      if (!this.disabled) {
        this.disabled = !this.disabled;
      }
    },
    /**
     * Show error in checkbox button
     */
    showError: function() {
      if (!this.error) {
        this.error = !this.error;
      }
    },
    /**
     * Show error in checkbox button
     */
    hideError: function() {
      if (this.error) {
        this.error = !this.error;
      }
    },
    /**
     * Toggles the checkbox when spacebar is pressed
     * @param {KeyboardEvent} e
     */
    _handleKeyDown: function(e) {
      if (e.keyCode === 32) {
        e.preventDefault();
        this.toggleCheck();
      }
    },

    /**
     * Fired when checkbox is toggled
     * @event cells-checkbox-button-toggled
     */
    /**
     * Fired when checkbox gets checked
     * @event cells-checkbox-button-checked
     */
    /**
     * Fired when checkbox gets unchecked
     * @event cells-checkbox-button-unchecked
     */
    /**
     * Fired when checkbox gets disabled
     * @event cells-checkbox-button-disabled
     */
    /**
     * Fired when checkbox gets enabled
     * @event cells-checkbox-button-enabled
     */
  });
}());
